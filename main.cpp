/*
 * Copyright (c) 2013, Kevin Läufer
 * Copyright (c) 2013-2014, Sascha Schade
 * Copyright (c) 2013, 2015-2017, Niklas Hauser
 *
 * This file is part of the modm project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
// ----------------------------------------------------------------------------

#include <modm/platform.hpp>
#include <modm/processing/timer.hpp>
#include <modm/architecture/interface/clock.hpp>
#include <modm/debug/logger.hpp>

#define MODM_BOARD_HAS_LOGGER

using namespace modm::literals;
struct SystemClock {

	static constexpr uint32_t Frequency = 96_MHz;
	static constexpr uint32_t Ahb = Frequency;
	static constexpr uint32_t Apb1 = Frequency / 4;
	static constexpr uint32_t Apb2 = Frequency / 1;

	static constexpr uint32_t Adc = Apb2;

	static constexpr uint32_t Can1   = Apb1;
	static constexpr uint32_t Can2   = Apb1;

	static constexpr uint32_t Spi1   = Apb2;
	static constexpr uint32_t Spi2   = Apb1;
	static constexpr uint32_t Spi3   = Apb1;
	static constexpr uint32_t Spi4   = Apb2;
	static constexpr uint32_t Spi5   = Apb2;
	static constexpr uint32_t Spi6   = Apb2;

	static constexpr uint32_t Usart1 = Apb2;
	static constexpr uint32_t Usart2 = Apb1;
	static constexpr uint32_t Usart3 = Apb1;
	static constexpr uint32_t Uart4  = Apb1;
	static constexpr uint32_t Uart5  = Apb1;
	static constexpr uint32_t Usart6 = Apb2;
	static constexpr uint32_t Uart7  = Apb1;
	static constexpr uint32_t Uart8  = Apb1;

	static constexpr uint32_t I2c1   = Apb1;
	static constexpr uint32_t I2c2   = Apb1;
	static constexpr uint32_t I2c3   = Apb1;

	static constexpr uint32_t Apb1Timer = Apb1 * 2;
	static constexpr uint32_t Apb2Timer = Apb2 * 1;
	static constexpr uint32_t Timer1  = Apb2Timer;
	static constexpr uint32_t Timer2  = Apb1Timer;
	static constexpr uint32_t Timer3  = Apb1Timer;
	static constexpr uint32_t Timer4  = Apb1Timer;
	static constexpr uint32_t Timer5  = Apb1Timer;
	static constexpr uint32_t Timer6  = Apb1Timer;
	static constexpr uint32_t Timer7  = Apb1Timer;
	static constexpr uint32_t Timer8  = Apb2Timer;
	static constexpr uint32_t Timer9  = Apb2Timer;
	static constexpr uint32_t Timer10 = Apb2Timer;
	static constexpr uint32_t Timer11 = Apb2Timer;
	static constexpr uint32_t Timer12 = Apb1Timer;
	static constexpr uint32_t Timer13 = Apb1Timer;
	static constexpr uint32_t Timer14 = Apb1Timer;

	static constexpr uint32_t Usb = 48_MHz;

	static bool inline
	enable()
	{
		modm::platform::Rcc::enableExternalCrystal();	// 8MHz
		const modm::platform::Rcc::PllFactors pllFactors{
			.pllM = 4,		// 8MHz / M=4 -> 2MHz
			.pllN = 192,	// 2MHz * N=168 -> 336MHz
			.pllP = 4,		// 336MHz / P=2 -> 168MHz = F_cpu
			.pllQ = 8		// 336MHz / Q=7 ->  48MHz = F_usb
		};
		modm::platform::Rcc::enablePll(modm::platform::Rcc::PllSource::ExternalCrystal, pllFactors);
		// set flash latency for 168MHz
		modm::platform::Rcc::setFlashLatency<Frequency>();
		// switch system clock to PLL output
		modm::platform::Rcc::enableSystemClock(modm::platform::Rcc::SystemClockSource::Pll);
		modm::platform::Rcc::setAhbPrescaler(modm::platform::Rcc::AhbPrescaler::Div1);
		// APB1 has max. 42MHz
		// APB2 has max. 84MHz
		modm::platform::Rcc::setApb1Prescaler(modm::platform::Rcc::Apb1Prescaler::Div4);
		modm::platform::Rcc::setApb2Prescaler(modm::platform::Rcc::Apb2Prescaler::Div1);
		// update frequencies for busy-wait delay functions
		modm::platform::Rcc::updateCoreFrequency<Frequency>();

		return true;
	}
};

// using inputPin = modm::platform::GpioInverted<modm::platform::GpioInputC13>;
// using outputPin = modm::platform::GpioD12;
using namespace std::chrono_literals;

using Rx = modm::platform::GpioInputB7;
using Tx = modm::platform::GpioOutputA15;
using Uart = modm::platform::Usart1;

// using LoggerDevice = modm::IODeviceWrapper< Uart, modm::IOBuffer::BlockIfFull >;

modm::IODeviceWrapper< Uart, modm::IOBuffer::BlockIfFull > loggerDevice;
modm::log::Logger modm::log::debug(loggerDevice);
modm::log::Logger modm::log::info(loggerDevice);
modm::log::Logger modm::log::warning(loggerDevice);
modm::log::Logger modm::log::error(loggerDevice);

using LedOrange = modm::platform::GpioOutputD13;	// User LED 3
using LedGreen  = modm::platform::GpioOutputD12;	// User LED 4
using LedRed    = modm::platform::GpioOutputD14;	// User LED 5
using LedBlue   = modm::platform::GpioOutputD15;	// User LED 6
using Button = modm::platform::GpioInputA0;

// using Leds = modm::platform::SoftwareGpioPort< LedGreen, LedBlue, LedRed, LedOrange >;

int main()
{	

	SystemClock::enable();
	modm::platform::SysTickTimer::initialize<SystemClock>();


	Uart::connect<Rx::Rx, Tx::Tx>();
	Uart::initialize<SystemClock, 115200_Bd>();
	// inputPin::setInput();
	// inputPin::setInputTrigger(modm::platform::Gpio::InputTrigger::RisingEdge);
	// inputPin::enableExternalInterrupt();
	// Leds::setOutput(modm::Gpio::Low);
	Button::setInput();
	Button::setInputTrigger(modm::platform::Gpio::InputTrigger::RisingEdge);
	Button::enableExternalInterrupt();

	LedOrange::setOutput(modm::Gpio::Low);
	LedRed::setOutput(modm::Gpio::Low);
	LedBlue::setOutput(modm::Gpio::Low);
	LedGreen::setOutput(modm::Gpio::Low);
	LedOrange::set();
	LedRed::set();

	// Use the logging streams to print some messages.
	//Change MODM_LOG_LEVEL above to enable or disable these messages
	MODM_LOG_DEBUG   << "debug"   << modm::endl;
	MODM_LOG_INFO    << "info"    << modm::endl;
	MODM_LOG_WARNING << "warning" << modm::endl;
	MODM_LOG_ERROR   << "error"   << modm::endl;

	uint32_t counter(0);
	// modm::PrecisePeriodicTimer tmr(0.500990s);
	// modm::PeriodicTimer tmrS(0.500990s);

	// uint32_t ms_counter{0};
	// uint32_t us_counter{0};

	while (true)
	{
		
		LedBlue::toggle();
		LedGreen::toggle();
		LedOrange::toggle();
		LedRed::toggle();
		MODM_LOG_INFO << "loop: " << counter++ << modm::endl;
		modm::delay(Button::read() ? 100ms : 500ms);
		// {
		// 	uint32_t ms = modm::Clock::now().time_since_epoch().count();
		// 	if (ms < ms_counter) {
		// 		MODM_LOG_ERROR << ms << " < " << ms_counter << modm::endl;
		// 	}
		// 	ms_counter = ms;
		// }{
		// 	uint32_t us = modm::PreciseClock::now().time_since_epoch().count();
		// 	if (us < us_counter) {
		// 		MODM_LOG_ERROR << us << " < " << us_counter << modm::endl;
		// 	}
		// 	us_counter = us;
		// }

		// if (tmr.execute())
		// {
		// 	outputPin::toggle();

		// 	MODM_LOG_INFO << "loop: " << counter++ << modm::endl;
		// }

		// if (tmrS.execute())
		// {
		// 	outputPin::toggle();
		// }
	}

	return 0;
}
